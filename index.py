from math import floor, sqrt
from random import randint
from typing import List, Tuple

MAX = pow(2, 64)
SIZE = 10000


def linear_congruential_generator():
    m = MAX
    a = 6364136223846793005
    c = 1442695040888963407
    n = randint(0, m - 1)
    while True:
        n = (a * n + c) % m
        yield n


def calculate_chi_square(values: List, buckets_count: int) -> float:
    # Normalize values into 0 - buckets_count - 1 range and compose histogram.
    histogram = {bucket_number: 0 for bucket_number in range(buckets_count)}
    max_value = MAX - 1
    for value in values:
        bucket_number = floor(buckets_count * value / max_value)
        histogram[bucket_number] += 1
    # Calculate chi-square statistic.
    chi_square = 0
    bucket_expected = SIZE / buckets_count
    for bucket_number in range(buckets_count):
        bucket_observed = histogram[bucket_number]
        chi_square += pow(bucket_observed - bucket_expected, 2) / bucket_expected
    return chi_square


def calculate_kolmogorov_smirnov_deviations(values: List) -> Tuple[float, float]:
    # Compose histogram and cumulative of observed values.
    histogram = {value: 0 for value in values}
    for value in values:
        histogram[value] += 1
    cumulative_histogram = {}
    count = 0
    for value in sorted(histogram.keys()):
        count += histogram[value]
        cumulative_histogram[value] = count
    # Define cumulative distribution functions.
    n = len(values)
    def empirical_cumulative_distribution(value: int) -> float:
        return cumulative_histogram[value] / n
    def theoretical_cumulative_distribution(value: int) -> float:
        return value / MAX
    # Calculate k+ and k- statistics.
    k_plus = sqrt(n) * max([empirical_cumulative_distribution(value) - theoretical_cumulative_distribution(value) for value in histogram.keys()])
    k_minus = sqrt(n) * max([theoretical_cumulative_distribution(value) - empirical_cumulative_distribution(value) for value in histogram.keys()])
    return k_plus, k_minus


"""
https://www.johndcook.com/Beautiful_Testing_ch10.pdf
http://www-users.math.umn.edu/~garrett/students/reu/pRNGs.pdf
"""
generator = linear_congruential_generator()
values = [next(generator) for _ in range(SIZE)]
buckets_count = 20
chi_square_value = calculate_chi_square(values, buckets_count)
k_plus_value, k_minus_value = calculate_kolmogorov_smirnov_deviations(values)
#print(values)
print('Rozmiar próby: ' + str(len(values)))
print('-----------------')
print('Wynik testu chi-kwadrat: %.4f' % chi_square_value)
print('Stopnie swobody chi-kwadrat: ' + str(buckets_count - 1))
print('Rozkład chi-kwadrat: https://people.smp.uq.edu.au/YoniNazarathy/stat_models_B_course_spring_07/distributions/chisqtab.pdf')
"""
V less than the 1% entry or greater than the 99% entry to be nonrandom
V between the 1% and 5% entries or between the 95% and 99% entries to be suspect
V between the 5% and 10% entries or between the 90% and 95% entries to be almost suspect
"""
print('-----------------')
print('Wynik testu Kolmogorov-Smirnov k+: %.4f' % k_plus_value)
print('Wynik testu Kolmogorov-Smirnov k-: %.4f' % k_minus_value)
print('Tabela Kolmogorov-Smirnov: http://www-users.math.umn.edu/~garrett/students/reu/pRNGs.pdf')
